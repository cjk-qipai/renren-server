FROM registry.cn-hangzhou.aliyuncs.com/chuangjike/nodejs:6.9.2
COPY sources.list /etc/apt/sources.list
RUN echo "deb http://us.archive.ubuntu.com/ubuntu/ precise-updates main restricted" | tee -a /etc/apt/sources.list.d/precise-updates.list
RUN apt-get -y update

#node
RUN npm config set registry https://registry.npm.taobao.org

#mongodb
RUN npm install mongodb@2.2.24
RUN npm install async@2.1.5

#utils
RUN npm install crypto

RUN npm install co
RUN npm install bluebird
RUN npm install ws@1.1.4
RUN npm install request@2.79.0
RUN npm install request-promise@4.1.1
RUN npm install -g mocha@3.2.0

RUN npm install seneca
RUN npm install seneca-as-promised
RUN npm install ioredis
RUN npm install ko-sleep
RUN npm install -g pm2@2.4.0
RUN npm install colors

USER root
#防火墙的设置
RUN apt-get install -y ufw
RUN sed -i '7,7 s/yes/no/g' /etc/default/ufw
RUN apt-get -y upgrade
#安装node内存泄漏工具
#RUN apt-get -y update
#RUN apt-get install -y python python2.7-dev
#RUN apt-get install -y build-essential
#RUN apt-get install -y make
#RUN npm install memwatch-next

RUN npm install node-schedule
RUN npm install blueimp-md5
#时区设置
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

CMD node /app/index.js
