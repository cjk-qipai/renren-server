const schedule = require("node-schedule");
const cp = require('child_process');
const co = require("co");

function exec(cmd) {
    return new Promise((resolve, reject) => {
        cp.exec(cmd, function(e, stdout, stderr) {
            if (e) {
                reject(e);
            }
            resolve({
                stderr: stderr,
                stdout: stdout
            });
        });
    });
}

co(function*(){
    let res = yield exec('ufw deny 80');
    console.log(res.stdout);
    console.log(res.stderr);
}).catch((err)=>{
    console.log(err);
});
//
// var rule = new schedule.RecurrenceRule();
// rule.hour = 21;
// var j = schedule.scheduleJob(rule, function() {
//     console.log("执行任务");
// });
