"use strict";
String.prototype.format = function(args) {
    var result = this;
    if (arguments.length > 0) {
        if (arguments.length == 1 && typeof(args) == "object") {
            for (var key in args) {
                if (args[key] != undefined) {
                    var reg = new RegExp("({" + key + "})", "g");
                    result = result.replace(reg, args[key]);
                }
            }
        } else {
            for (var i = 0; i < arguments.length; i++) {
                if (arguments[i] != undefined) {
                    var reg = new RegExp("({[" + i + "]})", "g");
                    result = result.replace(reg, arguments[i]);
                }
            }
        }
    }
    return result;
};

Date.prototype.format = function(fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "(h|H)+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
};
Array.prototype.shuffle = function() {
    let r = this.map((e) => {
        if (e instanceof Object) {
            let json = JSON.stringify(e);
            return JSON.parse(json);
        } else {
            return e;
        }
    });
    let length = r.length,
        temp,
        random;
    while (0 != length) {
        random = Math.floor(Math.random() * length);
        length--;
        temp = r[length];
        r[length] = r[random];
        r[random] = temp;
    }
    return r;
};
Array.prototype.pushAll = function(arr) {
    for (let a of arr) {
        this.push(a);
    }
};
Array.prototype.clone = function() {
    return this.map((e) => {
        return e;
    });
};
Array.prototype.insertAt = function(index, value) {
    var part1 = this.slice(0, index);
    var part2 = this.slice(index);
    part1.push(value);
    return (part1.concat(part2));
};

Array.prototype.removeAt = function(index) {
    index++;
    var part1 = this.slice(0, index);
    var part2 = this.slice(index);
    part1.pop();
    return (part1.concat(part2));
};
Array.prototype.remove = function(val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};
Array.prototype.last = function() {
    return this[this.length - 1];
};
Array.prototype.contains = function(e, mapFn) {
    if (mapFn) {
        let arr = this.map(mapFn);
        return arr.indexOf(mapFn(e)) != -1;
    } else {
        return this.indexOf(e) != -1;
    }

};
Array.prototype.containsAll = function(arr, mapFn) {
    if (mapFn) {
        let mapThis = this.map(mapFn);
        let mapArr = arr.map(mapFn);
        for (let e of mapArr) {
            if (!mapThis.contains(e)) {
                return false;
            }
        }
        return true;
    } else {
        for (let e of arr) {
            if (!this.contains(e)) {
                return false;
            }
        }
        return true;
    }
};
Array.times = function(value, n) {
    let arr = [];
    for (let i = 0; i < n; i++) {
        arr.push(value);
    }
    return arr;
};
