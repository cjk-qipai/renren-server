const Action = function() {
    this.userId;
    this.roomNumber;
    this.gameId;
    this.zimoCard;//自摸的那张牌
    this.myCards; //我的牌，比如我手中2,4万
    this.otherCard; //别人的牌，只能是一张，比如3万
    this.tingCards;//如果是听牌类型，这里保存打了之后可以听牌的牌
    this.type; //操作类型，比如碰，吃，杠，接炮，自摸
    this.status = STATUS_WAIT;
    this.menzis; //如果是胡牌，要返回所有的门子数量
    this.point; //门子的点数
    this.isGangBao;
    this.isQiangGang;
    this.isBaoTing;
    this.isTianHu;//如果是自摸，可以有天胡
    this.isHaiDiGuo;//海底的时候不能打牌了，所以加杠或者暗杠需要加入过的操作
};
Action.ACTIONS_MO = "mo";
Action.ACTIONS_CHI = "chi";
Action.ACTIONS_PENG = "peng";
Action.ACTIONS_ANGANG = "anGang";
Action.ACTIONS_MINGGANG = "mingGang";
Action.ACTIONS_JIAGANG = "jiaGang";
Action.ACTIONS_TING = "ting";
Action.ACTIONS_ZIMO = "zimo";
Action.ACTIONS_DA = "da";
Action.ACTIONS_JIEPAO = "jiepao";
Action.ACTIONS_GUO = "guo";

Action.STATUS_WAIT = 1; //代表这是一个候选操作
Action.STATUS_CONFIRM = 2; //代表这是一个选定了的操作
Action.STATUS_PENDING = 3;//这是一个挂起的操作，比如说，加杠时，需要判断别人是否抢杠，那么这个操作就被挂起

module.exports = Action;
