"use strict";
require("../enhance.js");
let Card = function() {
    this.value; //牌的值
    this.userId; //在谁手中,null代表还没有被摸
    this.roomNumber; //属于哪个房间
    this.status = Card.STATUS_INIT;
    this.cantDa; //已经报听的牌，这个牌不能打
};
Card.prototype.equal = function(card) {
    if (this.value == null || card.value == null) {
        return false;
    }
    return this.value == card.value;
};

Card.getType = function(card) {
    return parseInt(card.value / 10);
};

Card.Alpha = {};
Card.Alpha.MENZI_XUEYUAN = "学员";
Card.Alpha.MENZI_XUESHI = "学士";
Card.Alpha.MENZI_SHUOSHI = "硕士";
Card.Alpha.MENZI_BOSHI = "博士";
Card.Alpha.MENZI_DAOSHI = "导师";

Card.Beta = {};
Card.Beta.MENZI_PINGHU = "平胡";
Card.Beta.MENZI_DAPENGDUI = "大碰对";
Card.Beta.MENZI_QINGYISE = "清一色";
Card.Beta.MENZI_QIXIAODUI = "七小对";
Card.Beta.MENZI_SQIXIAODUI = "龙七对";
Card.Beta.MENZI_LIHU = "利行天下";
Card.Beta.MENZI_LEHU = "乐得天下";
Card.Beta.MENZI_DINGHU = "和定天下";
Card.Beta.MENZI_LONGFENG = "龙凤呈祥";

Card.MENZI_TIANHU = "天胡";
Card.MENZI_HAIDI = "海底";
Card.MENZI_GANGBAO = "杠爆";
Card.MENZI_QIANGGANG = "抢杠胡";


Card.STATUS_INIT = 1; //还没有被摸的牌
Card.STATUS_ONHAND = 2; //手上的牌
Card.STATUS_BEIDA = 3; //已经被打出
Card.STATUS_PENG = 4; //牌被碰
Card.STATUS_BIRD = 5; //是扎鸟的牌
Card.STATUS_MINGGANG = 6; //牌被明杠
Card.STATUS_ANGANG = 7; //牌被暗杠
Card.STATUS_JIAGANG = 8; //牌被加杠

Card.A = 1;
Card.B = 2;
Card.C = 3;
Card.D = 4;
Card.E = 5;
Card.F = 6;
Card.G = 7;

Card.H = 11;
Card.I = 12;
Card.J = 13;
Card.K = 14;
Card.L = 15;
Card.M = 16;
Card.N = 17;

Card.O = 21;
Card.P = 22;
Card.Q = 23;

Card.R = 31;
Card.S = 32;
Card.T = 33;

Card.U = 41;
Card.V = 42;
Card.W = 43;

Card.X = 51;
Card.Y = 52;
Card.Z = 53;

Card.allCards = (() => {
    let tmpCards = [];
    for (let i = 0; i < 4; i++) {
        for (let i = 65; i <= 90; i++) {
            tmpCards.push({
                value: Card[String.fromCharCode(i)],
                status: Card.STATUS_INIT
            });
        }
    }
    return tmpCards;
})();

Card.cards = (() => {
    let tmpCards = [];
    for (let i = 65; i <= 90; i++) {
        tmpCards.push({
            value: Card[String.fromCharCode(i)],
            status: Card.STATUS_INIT
        });
    }
    return tmpCards;
})();

module.exports = Card;
