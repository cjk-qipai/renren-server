const Game = function() {
    this.roomNumber; //属于哪个房间
    this.result; //结算信息
};
Game.Result = function() {
    this.cards; //这一局最终所有的牌
    this.type; //结果的类型
    this.hupais;//如果是接炮，这个属性保存放炮的信息
};
Game.Result.TYPE_LIUJU = "liuju";
Game.Result.TYPE_JIEPAO = "jiepao";
Game.Result.TYPE_ZIMO = "zimo";
module.exports = Game;
