var Room = function() {
    this.number;
    this.direction = Room.DIRECTION_EAST;
    this.owerId;
    this.host;
    this.currentGameId;
    this.currentGameIndex; //当前打到第几局
    this.maxGame; //最多能打多少局
    this.birdCount;
    this.maxBase; //最大倍数
    this.isQiangGangFirst; //是否抢杠优先
    this.mode = Room.MODE_ALPHA;
    this.lastCard;
};
Room.DEFAUL_MAX_GAME = 3;
Room.DIRECTION_EAST = 1; //东
Room.DIRECTION_SOUTH = 2; //南
Room.DIRECTION_WEST = 3; //西
Room.DIRECTION_NORTH = 4; //北
Room.MODE_ALPHA = 1;
Room.MODE_BETA = 2;

Room.DIRECTIONS = [Room.DIRECTION_EAST, Room.DIRECTION_SOUTH, Room.DIRECTION_WEST, Room.DIRECTION_NORTH];
Room.nextDirection = function(curDirection) {
    if (curDirection == Room.DIRECTION_NORTH) {
        return Room.DIRECTION_EAST;
    }
    return curDirection + 1;
};
module.exports = Room;
