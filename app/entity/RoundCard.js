"use strict";
require("../enhance.js");
//当前这一局临时的牌，这一局结束后要删除
let RoundCard = function() {
    this.value; //牌的值
    this.userId; //在谁手中,null代表还没有被摸
    this.roomNumber; //属于哪个房间
    this.direction;//这个牌是哪个方位的人出的
    this.index;//这个牌是第多少张出的
};
