var User = function() {
    this.unionid;
    this.point;
    this.roomNumber; //房间号
    this.roomDirection; //所在方向
    this.roomCard = 0; //房卡数量
    this.sex;
    this.nickname;
    this.headimgurl;
    this.bonus = 0; //商城积分
    this.isBaoTing;
    this.gamePoint; //当前局的分数，下一局要清空的
    this.isReady; //是否准备好
    this.fangpaoCount;//放炮次数
    this.jiePaoCount;//接炮次数
    this.zimoCount;//自摸次数
    this.isAgreeDismiss;//是否投了同意解散
};
module.exports = User;
