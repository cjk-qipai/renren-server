const EventEmitter = require('events').EventEmitter;
module.exports = (() => {
    return {
        DEV_STATUS_PRO: "pro",
        DEV_STATUS_UNITTEST: "unittest",
        DEV_STATUS_TEST: "test",
        wsUsers: new Map(),
        event: new EventEmitter(),
        colls: {
            users: "users",
            rooms: "rooms",
            logs: "logs",
            cards: "cards",
            nonces: "nonces",
            roundCards: "roundCards", //一圈牌
            actions: "actions",
            games: "games",
            diaodiaoshous: "diaodiaoshous",
            hupais: "hupais",
            gangs: "gangs", //杠的信息表
            records: "records",
            recordDetails: "recordDetails",
            guoShous: "guoShous",
            agents: "agents",
            userinfos: "userinfos",
            tings: "tings",
            notices: "notices",
            tishis: "tishis"
        }
    };
})();
