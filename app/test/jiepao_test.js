const assert = require("assert");

const majiang = require("../yuanjiangLogic.js");
const Card = require("../entity/Card.js");
const Action = require("../entity/Action.js");
describe("接炮判断", () => {
    it("学员不可以接炮", () => {
        let res = majiang.alpha.canJiePao([{
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.B
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.C
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }], {
            status: Card.STATUS_ONHAND,
            value: Card.E
        });
        assert.equal(res, false);
    });
    it("学员不可以抢杠胡", () => {
        let res = majiang.alpha.canJiePao([{
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.B
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.C
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }], {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, false, true);
        assert.equal(res, false);
    });
    it("学员抢杠胡", () => {
        let res = majiang.alpha.jiepao([{
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.B
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.C
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }], {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, false, true);
        assert.equal(res.menzis.length, 1);
        assert.equal(res.menzis[0], Card.MENZI_QIANGGANG);
    });
    it("学士抢杠胡", () => {
        let res = majiang.alpha.jiepao([{
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }], {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, false, true);
        assert.equal(res.menzis.length, 2);
        assert.equal(res.menzis[0], Card.Alpha.MENZI_XUESHI);
        assert.equal(res.menzis[1], Card.MENZI_QIANGGANG);
    });
    it("博士", () => {
        let res = majiang.alpha.jiepao([{
            status: Card.STATUS_ONHAND,
            value: Card.H
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.I
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.J
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.K
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.L
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.M
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.N
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.Q
        }], {
            status: Card.STATUS_ONHAND,
            value: Card.Z
        }, false, false);
        assert.equal(res.menzis.length, 1);
        assert.equal(res.menzis[0], Card.Alpha.MENZI_BOSHI);
    });
    it("导师", () => {
        let res = majiang.alpha.jiepao([{
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.I
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.O
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.U
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.H
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.L
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.Q
        }], {
            status: Card.STATUS_ONHAND,
            value: Card.Z
        }, false, false);
        assert.equal(res.menzis.length, 1);
        assert.equal(res.menzis[0], Card.Alpha.MENZI_DAOSHI);
    });
    it("导师抢杠胡", () => {
        let res = majiang.alpha.jiepao([{
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.I
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.O
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.U
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.H
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.L
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.Q
        }], {
            status: Card.STATUS_ONHAND,
            value: Card.Z
        }, false, true);
        assert.equal(res.menzis.length, 2);
        assert.equal(res.menzis[0], Card.Alpha.MENZI_DAOSHI);
        assert.equal(res.menzis[1], Card.MENZI_QIANGGANG);
    });
});
