const assert = require("assert");

const majiang = require("../yuanjiangLogic.js");
const Card = require("../entity/Card.js");
const Action = require("../entity/Action.js");
describe("测试碰牌", () => {
    it("有对子可以碰", () => {
        let cards = [{
            value: Card.A,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.B,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.C,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.D,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.D,
            status: Card.STATUS_ONHAND
        }];
        let res = majiang.alpha.canPeng(cards, {
            value: Card.D
        });
        assert.equal(res.actions.length, 1);
        assert.equal(res.actions[0].myCards[0].value, Card.D);
    });
    it("三个的可以碰", () => {
        let cards = [{
            value: Card.Q,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.Q,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.Q,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.Q,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.B,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.B,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.B,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.D,
            status: Card.STATUS_ONHAND
        }];
        let res = majiang.alpha.canPeng(cards, {
            value: Card.B
        });
        assert.equal(res.actions.length, 1);
        assert.equal(res.actions[0].myCards[0].value, Card.B);
    });
    it("只能用手里的牌碰", () => {
        let cards = [{
            value: Card.A,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.B,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.C,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.D,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.D,
            status: Card.STATUS_PENG
        }];
        let res = majiang.alpha.canPeng(cards, {
            value: Card.D
        });
        assert.equal(res.actions.length, 0);
    });
    it("牌小于3张不能碰牌", () => {
        let cards = [{
            value: Card.A,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.B,
            status: Card.STATUS_ONHAND
        }];
        let res = majiang.alpha.canPeng(cards, {
            value: Card.B
        });
        assert.equal(res.actions.length, 0);
    });
    it("牌小于4张不能碰牌2", () => {
        let cards = [{
            value: Card.A,
            status: Card.STATUS_ONHAND
        }];
        let res = majiang.alpha.canPeng(cards, {
            value: Card.A
        });
        assert.equal(res.actions.length, 0);
    });
    it("可以碰", () => {
        let cards = [{
            value: Card.A,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.A,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.B,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.B,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.C,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.C,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.D,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.D,
            status: Card.STATUS_ONHAND
        }];
        let res = majiang.alpha.canPeng(cards, {
            value: Card.A,
            status: Card.STATUS_ONHAND
        });
        assert.equal(res.actions.length, 1);
    });
    it("可以碰2", () => {
        let cards = [{
            value: Card.H,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.H,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.I,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.I,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.J,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.J,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.K,
            status: Card.STATUS_ONHAND
        }, {
            value: Card.K,
            status: Card.STATUS_ONHAND
        }];
        let res = majiang.alpha.canPeng(cards, {
            value: Card.H,
            status: Card.STATUS_ONHAND
        });
        assert.equal(res.actions.length, 1);
    });
});
