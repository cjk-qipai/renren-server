const assert = require("assert");

const majiang = require("../yuanjiangLogic.js");
const Card = require("../entity/Card.js");
const Action = require("../entity/Action.js");
describe("自摸判断", () => {
    it("学员可以自摸", () => {
        let res = majiang.alpha.canZiMo([{
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.B
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.C
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }]);
        assert.equal(res, true);
    });
    it("学员可以自摸2", () => {
        let res = majiang.alpha.canZiMo([{
            status: Card.STATUS_PENG,
            value: Card.G
        }, {
            status: Card.STATUS_PENG,
            value: Card.G
        }, {
            status: Card.STATUS_PENG,
            value: Card.G
        }, {
            status: Card.STATUS_PENG,
            value: Card.O
        }, {
            status: Card.STATUS_PENG,
            value: Card.O
        }, {
            status: Card.STATUS_PENG,
            value: Card.O
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.Y
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.Z
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.X
        }]);
        assert.equal(res, true);
    });
    it("学员自摸", () => {
        let res = majiang.alpha.zimo([{
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.B
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.C
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }], []);
        assert.equal(res.menzis.length, 2);
        assert.equal(res.menzis[0], Card.Alpha.MENZI_XUEYUAN);
        assert.equal(res.menzis[1], Card.MENZI_HAIDI);
    });
    it("学士杠爆", () => {
        let res = majiang.alpha.zimo([{
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.D
        }, {
            status: Card.STATUS_BEIDA,
            value: Card.E
        }, {
            status: Card.STATUS_BEIDA,
            value: Card.E
        }, {
            status: Card.STATUS_BEIDA,
            value: Card.E
        }], [{
            status: Card.STATUS_INIT,
            value: Card.E
        }], false, false, true);
        assert.equal(res.menzis.length, 2);
        assert.equal(res.menzis[0], Card.Alpha.MENZI_XUESHI);
        assert.equal(res.menzis[1], Card.MENZI_GANGBAO);
    });
    it("博士", () => {
        let res = majiang.alpha.zimo([{
            status: Card.STATUS_ONHAND,
            value: Card.H
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.I
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.J
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.K
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.L
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.M
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.N
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.Q
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.Z
        }], [{
            status: Card.STATUS_INIT,
            value: Card.E
        }]);
        assert.equal(res.menzis.length, 1);
        assert.equal(res.menzis[0], Card.Alpha.MENZI_BOSHI);
    });
    it("导师", () => {
        let res = majiang.alpha.zimo([{
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.I
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.O
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.U
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.H
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.L
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.Q
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.Z
        }], [{
            status: Card.STATUS_INIT,
            value: Card.E
        }]);
        assert.equal(res.menzis.length, 1);
        assert.equal(res.menzis[0], Card.Alpha.MENZI_DAOSHI);
    });
    it("导师杠爆", () => {
        let res = majiang.alpha.zimo([{
            status: Card.STATUS_ONHAND,
            value: Card.A
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.E
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.I
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.O
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.U
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.H
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.L
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.Q
        }, {
            status: Card.STATUS_ONHAND,
            value: Card.Z
        }], [{
            status: Card.STATUS_INIT,
            value: Card.E
        }], undefined, undefined, true);
        assert.equal(res.menzis.length, 2);
        assert.equal(res.menzis[0], Card.Alpha.MENZI_DAOSHI);
        assert.equal(res.menzis[1], Card.MENZI_GANGBAO);
    });
});
